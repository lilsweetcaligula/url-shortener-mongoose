const assert = require('assert-plus')

const ExpressUtils = {
  asyncHandler(handler) {
    assert.func(handler, 'handler')

    return (...args) => {
      const p = handler(...args)
        
      if (p && p.constructor === Promise) {
        const [next,] = args.slice().reverse()

        if (typeof next !== 'function') {
          return p
        }

        return p.catch(next)
      }

      throw new Error('Expected the handler to be async, i.e. return a promise.')
    }
  }
}

module.exports = ExpressUtils
