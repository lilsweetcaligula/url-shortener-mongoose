const assert = require('assert-plus')

const Random = {
  genString: opts => {
    assert.object(opts, 'opts')
    assert.number(opts.length, 'opts.length')

    const POOL = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    let acc = ''

    for (let i = 0; i < opts.length; i++) {
      acc += Random.choice(POOL)
    }

    return acc
  },

  choice: iter => {
    assert(iter[Symbol.iterator], 'iter must be iterable')

    const ary = [...iter]
    const i = Math.floor(Math.random() * ary.length)

    return ary[i]
  }
}

module.exports = Random
