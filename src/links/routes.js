const Express = require('express')
const ShowLink = require('./show/handler.js')
const CreateLink = require('./create/handler.js')

exports.initialize = () => 
  Express.Router()
    .get('/:key', ShowLink.initialize())
    .post('/', CreateLink.initialize())

