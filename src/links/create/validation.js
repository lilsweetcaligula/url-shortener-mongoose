const assert = require('assert-plus')
const ValidUrl = require('valid-url')
const { validateWith } = require('../../lib/validation.js')
const { ValidationError } = require('../../lib/errors.js')

const Validation = {
  validateBody: async (unsafe_body) => {
    const validateSchema = validateWith(schema => schema.object({
      url: schema.string().required()
    }))

    const validateUrl = body => {
      assert('url' in body, 'body.url')

      const { url } = body

      if (!ValidUrl.isUri(url)) {
        throw new ValidationError('"url" is not a valid url')
      }

      return body
    }

    return validateUrl(await validateSchema(unsafe_body))
  }
}

module.exports = Validation
