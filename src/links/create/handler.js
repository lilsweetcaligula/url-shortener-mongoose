const assert = require('assert-plus')
const { asyncHandler } = require('../../lib/express_utils.js')
const { validateBody } = require('./validation.js')
const Utils = require('./utils.js')
const Link = require('../../models/link.js')

exports.initialize = () => asyncHandler(async (req, res) => {
  const { body: unsafe_body } = req
  const safe_body = await validateBody(unsafe_body)

  assert('url' in safe_body, 'safe_body.url')

  const { url } = safe_body
  const key = Utils.genKeyForLink()
  
  const new_link = await Link.create({ url, key })

  const short_url = Utils.makeShortUrl({
    proto: req.protocol,
    key: key
  })

  res.status(201).json({ key, short_url })

  return
})

