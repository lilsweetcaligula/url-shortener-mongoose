const assert = require('assert-plus')
const config = require('../../config.js')
const { genString } = require('../../lib/random_utils.js')

const Utils = {
  genKeyForLink: () => genString({ length: 7 }),

  makeShortUrl: params => {
    assert.object(params, 'params')

    const { proto, key } = params

    assert.string(proto, 'proto')
    assert.string(key, 'key')

    const hostname = config.getServerHostname()

    return proto + '://' + hostname + '/' + key
  }
}

module.exports = Utils
