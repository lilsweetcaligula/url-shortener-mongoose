const assert = require('assert-plus')
const Link = require('../../models/link.js')
const Mongo = require('../../lib/mongo.js')
const { asyncHandler } = require('../../lib/express_utils.js')

exports.initialize = () => asyncHandler(async (req, res) => {
  assert('key' in req.params, 'req.params.key')

  const { key } = req.params
  const link = await Link.findOne({ key })

  if (!link) {
    res.sendStatus(404)
    return
  }

  const { url: redirect_to } = link

  res.redirect(301, redirect_to)

  return
})

