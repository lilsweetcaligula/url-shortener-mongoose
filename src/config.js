const assert = require('assert-plus')

const config = {
  getServerPort: () => config.fromEnv('SERVER_PORT'),

  getServerHost: () => config.fromEnv('SERVER_HOST'),

  getMongoUser: () => config.fromEnv('MONGO_USER'),

  getMongoPassword: () => config.fromEnv('MONGO_PASSWORD'),

  getMongoHost: () => config.fromEnv('MONGO_HOST'),

  getMongoPort: () => config.fromEnv('MONGO_PORT'),

  getServerHostname: () => config.fromEnv('SERVER_HOSTNAME'),

  fromEnv: env_var => {
    assert.string(env_var, 'env_var')

    if (env_var in process.env) {
      return process.env[env_var]
    }

    throw new Error('process.env["' + env_var + '"] is not defined')
  }
}

module.exports = config
