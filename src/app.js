const Express = require('express')
const LinksApi = require('./links/routes.js')
const { handleAppError } = require('./middleware/handle_app_error.js')

exports.initialize = () =>
  Express()
    .use(Express.json())
    .use('/', LinksApi.initialize())
    .use(handleAppError())
