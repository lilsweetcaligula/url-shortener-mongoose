const assert = require('assert-plus')
const http = require('http')
const App = require('./app.js')
const Mongo = require('./lib/mongo.js')
const config = require('./config.js')

exports.initialize = () => {
  const app = makeApp()
  const server = makeServer(app)

  void connectToMongo()

  return server
}

const makeApp = (_ctx) => App.initialize() 

const makeServer = app => {
  const host = config.getServerHost()
  const port = config.getServerPort()

  const server = http.createServer(app)

  server.listen(port, host, () => {
    console.log('Server listening at address: %j', server.address())
  })
  
  return server
}

const connectToMongo = async () => {
  const user = config.getMongoUser()
  const password = config.getMongoPassword()
  const host = config.getMongoHost()
  const port = config.getMongoPort()

  await Mongo.connect({ user, password, host, port })
}

