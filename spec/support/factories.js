const assert = require('assert-plus')
const Link = require('../../src/models/link.js')

exports.createLink = (attrs = {}) => {
  assert.object(attrs, 'attrs')

  const defaults = {
    url: 'https://google.com',
    key: 'HJ34Fbv'
  }

  return Link.create({ ...defaults, ...attrs })
}

