const { makeShortUrl } = require('../../../src/links/create/utils.js')
const config = require('../../../src/config.js')

describe('utilities for the POST / handler', () => {
  describe('makeShortUrl', () => {
    const hostname = '0.0.0.0:1337'

    beforeEach(() => {
      spyOn(config, 'getServerHostname').and.returnValue(hostname)
    })

    it('builds a short url', () => {
      const proto = 'https'
      const key = 'foobar'

      const result = makeShortUrl({ proto, key })

      expect(result).toEqual('https://0.0.0.0:1337/foobar')
      expect(config.getServerHostname).toHaveBeenCalled()
    })
  })
})
