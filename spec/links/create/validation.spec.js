const { validateBody } = require('../../../src/links/create/validation.js')
const { expectRejected } = require('../../support/matchers.js')(jasmine)
const { ValidationError } = require('../../../src/lib/errors.js')

describe('POST / utils', () => {
  describe('validateBody', () => {
    const buildParams = () => ({
      url: 'https://google.com'
    })

    it('requires the "url" param', async () => {
      const body = buildParams(); delete body.url

      await expectRejected(validateBody(body),
        new ValidationError('"url" is required'))
    })

    it('requires the "url" param to be a string', async () => {
      const body = { ...buildParams(), url: 123 }

      await expectRejected(validateBody(body),
        new ValidationError('"url" must be a string'))
    })

    it('disallows unknown params', async () => {
      const body = { ...buildParams(), unknown: 'param' }

      await expectRejected(validateBody(body),
        new ValidationError('"unknown" is not allowed'))
    })

    it('passes the validation with valid params', async () => {
      const body = buildParams()
      const result = await validateBody(body)

      expect(result).toEqual(body)
    })
  })
})

