const mongoose = require('mongoose')
const Mongo = require('../../src/lib/mongo.js')

describe('mongo utilities', () => {
  describe('connect', () => {
    it('connects to the mongodb instance', async () => {
      const user = 'frank'
      const password = 'alotofcoffeeinbrazil'
      const host = 'mongo_foo'
      const port = 27917
      const params = { user, password, host, port }

      const fake_conn = { _fake: 'connection' }

      spyOnProperty(mongoose, 'connection', 'get').and.returnValue(fake_conn)
      spyOn(mongoose, 'connect').and.returnValue(Promise.resolve())

      const result = await Mongo.connect(params)

      expect(result).toEqual(fake_conn)
    })
  })
})
