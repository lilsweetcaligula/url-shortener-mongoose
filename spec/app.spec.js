const supertest = require('supertest')
const Mongo = require('./support/mongo.js')
const App = require('../src/app.js')
const Link = require('../src/models/link.js')
const Factories = require('./support/factories.js')

describe('App', () => {
  afterEach(async () => {
    await Mongo.clean()
  })

  describe('.initialize', () => {
    it('initializes the application', () => {
      const app = App.initialize()
      expect(app).toEqual(jasmine.any(Function))
    })
  })

  describe('requesting a link', () => {
    const requestLink = id => supertest(App.initialize())
      .get('/' + id)

    describe('when the link does not exist', () => {
      beforeEach(async () => {
        await Factories.createLink()
      })

      it('responds with 404', async () => {
        await requestLink('abcd')
          .expect(404)
      })
    })

    describe('on success', () => {
      let link_mongo_id

      const link_key = 'HY1j3O7'
      const redirect_to = 'https://127.0.0.1/foo/bar/baz'

      beforeEach(async () => {
        link_mongo_id = (await Factories.createLink({
          url: redirect_to,
          key: link_key
        }))._id
      })

      it('redirects to the url specified in the link based on the key', async () => {
        await requestLink(link_key)
          .expect(301)
          .expect(res => {
            expect(res.headers.location).toEqual(redirect_to)
          })
      })
    })
  })

  describe('creating a link', () => {
    const HandlerUtils = require('../src/links/create/utils.js')

    const requestCreateLink = id => supertest(App.initialize())
      .post('/')

    const buildBody = () => ({
      url: 'https://google.com'
    })

    describe('when given bad data', () => {
      it('responds with 400 Bad Data', async () => {
        const bad_data = { ...buildBody(), unknown: 'prop' }

        await requestCreateLink()
          .send(bad_data)
          .expect(400)
      })

      it('responds with 400 Bad Data if the url is malformed', async () => {
        const bad_data = { ...buildBody(), url: 'not_a_real_url' }

        await requestCreateLink()
          .send(bad_data)
          .expect(400)
      })
    })

    describe('on success', () => {
      const fake_short_url = 'fake_short_url'

      beforeEach(() => {
        spyOn(HandlerUtils, 'makeShortUrl')
          .and.returnValue(fake_short_url)
      })

      it('creates a new link', async () => {
        const ok_data = buildBody()

        expect(await Link.countDocuments()).toEqual(0)

        await requestCreateLink()
          .send(ok_data)
          .expect(201)
          .expect(async (res) => {
            expect(await Link.countDocuments()).toEqual(1)
          })
      })

      it('responds with the key of the link', async () => {
        const ok_data = buildBody()

        await requestCreateLink()
          .send(ok_data)
          .expect(async (res) => {
            const new_link = await Link.findOne()

            expect(res.body).toEqual(jasmine.objectContaining({
              key: new_link.key
            }))
          })
      })

      describe('short url', () => {
        it('is included in the response', async () => {
          const ok_data = buildBody()

          await requestCreateLink()
            .send(ok_data)
            .expect(async (res) => {
              const new_link = await Link.findOne()

              expect(res.body).toEqual(jasmine.objectContaining({
                short_url: fake_short_url
              }))

              expect(HandlerUtils.makeShortUrl).toHaveBeenCalledWith({
                proto: jasmine.any(String),
                key: new_link.key
              })
            })
        })
      })

      it('creates a new link with the correct url value', async () => {
        const expected_url = 'https://wikipedia.org'
        const ok_data = { ...buildBody(), url: expected_url }

        await requestCreateLink()
          .send(ok_data)
          .expect(async (res) => {
            const new_link = await Link.findOne()
            
            expect(new_link.url).toEqual(expected_url)
            expect(new_link.key).toEqual(jasmine.any(String))
          })
      })
    })
  })
})

